package net.kothar.jsonhash;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.databind.node.ValueNode;

import net.kothar.jsonhash.patricia.PatriciaTrie;

public class PatriciaTrieTest {

	@Test
	public void can_add_string() throws JsonProcessingException {
		PatriciaTrie trie = new PatriciaTrie();

		String path = "foo/bar";
		TextNode value = JsonNodeFactory.instance.textNode("baz");
		trie.put(path, value);

		System.out.println("Hash: " + trie.md5String());
		System.out.println(trie);

		ValueNode storedValue = trie.get(path);
		assertEquals(value, storedValue);
	}

	@Test
	public void can_add_strings() throws JsonProcessingException {

		PatriciaTrie trie1 = new PatriciaTrie();
		TextNode value = JsonNodeFactory.instance.textNode("baz");
		trie1.put("foo/bar", value);
		trie1.put("foo/foo", value);

		System.out.println("Hash: " + trie1.md5String());
		System.out.println(trie1);

		PatriciaTrie trie2 = new PatriciaTrie();
		trie2.put("foo/foo", value);
		trie2.put("foo/bar", value);

		System.out.println("Hash: " + trie2.md5String());
		System.out.println(trie2);

		PatriciaTrie trie3 = new PatriciaTrie();
		trie3.put("foo/foo", value);
		trie3.put("foo/bar", value);
		trie3.put("foo/bar/baz", value);

		System.out.println("Hash: " + trie3.md5String());
		System.out.println(trie3);

		assertArrayEquals(trie1.md5(), trie2.md5());
		assertNotEquals(trie1.md5String(), trie3.md5String());

		ValueNode storedValue = trie3.get("foo/bar");
		assertEquals(value, storedValue);
	}

	@Test
	public void can_add_number() throws JsonProcessingException {
		PatriciaTrie trie = new PatriciaTrie();

		String path = "foo/bar";
		NumericNode value = JsonNodeFactory.instance.numberNode(53.4);
		trie.put(path, value);

		System.out.println("Hash: " + trie.md5String());
		System.out.println(trie);

		ValueNode storedValue = trie.get(path);
		assertEquals(value, storedValue);

	}

	@Test
	public void can_add_multiple() throws JsonProcessingException {
		PatriciaTrie trie = new PatriciaTrie();

		String prefix = "n/";
		for (int i = 0; i < 100; i++) {
			NumericNode value = JsonNodeFactory.instance.numberNode(i);
			trie.put(prefix + i, value);
		}

		System.out.println("Hash: " + trie.md5String());
		System.out.println(trie);

		ValueNode storedValue = trie.get(prefix + 50);
		assertEquals(50, storedValue.intValue());
	}
}
