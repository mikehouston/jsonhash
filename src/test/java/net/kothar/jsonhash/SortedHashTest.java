package net.kothar.jsonhash;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import net.kothar.jsonhash.sorted.SortedHash;

public class SortedHashTest {

	private ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

	@Test
	public void can_add_strings() throws JsonProcessingException {

		ObjectNode node1 = JsonNodeFactory.instance.objectNode();
		TextNode value = JsonNodeFactory.instance.textNode("baz");
		node1.set("foo/bar", value);
		node1.set("foo/foo", value);

		System.out.println("Hash: " + SortedHash.hashString(node1));
		System.out.println(mapper.writeValueAsString(node1));

		ObjectNode node2 = JsonNodeFactory.instance.objectNode();
		node2.set("foo/foo", value);
		node2.set("foo/bar", value);

		System.out.println("Hash: " + SortedHash.hashString(node2));
		System.out.println(mapper.writeValueAsString(node2));

		ObjectNode node3 = JsonNodeFactory.instance.objectNode();
		node3.set("foo/bar", value);
		node3.set("foo/foo", value);
		node3.set("foo/bar/baz", value);

		System.out.println("Hash: " + SortedHash.hashString(node3));
		System.out.println(mapper.writeValueAsString(node3));

		assertArrayEquals(SortedHash.hash(node1), SortedHash.hash(node2));
		assertNotEquals(SortedHash.hashString(node1), SortedHash.hashString(node3));

		JsonNode storedValue = node3.get("foo/bar");
		assertEquals(value, storedValue);
	}

}
