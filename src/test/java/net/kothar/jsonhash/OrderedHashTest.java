package net.kothar.jsonhash;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import net.kothar.jsonhash.ordered.OrderedJsonNodeFactory;

public class OrderedHashTest {

	private ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

	@Test
	public void can_add_strings() throws JsonProcessingException, NoSuchAlgorithmException {

		ObjectNode node1 = OrderedJsonNodeFactory.instance.objectNode();
		TextNode value = OrderedJsonNodeFactory.instance.textNode("baz");
		node1.set("foo/bar", value);
		node1.set("foo/foo", value);

		System.out.println("Hash: " + hashString(node1));
		System.out.println(mapper.writeValueAsString(node1));

		ObjectNode node2 = OrderedJsonNodeFactory.instance.objectNode();
		node2.set("foo/foo", value);
		node2.set("foo/bar", value);

		System.out.println("Hash: " + hashString(node2));
		System.out.println(mapper.writeValueAsString(node2));

		ObjectNode node3 = OrderedJsonNodeFactory.instance.objectNode();
		node3.set("foo/bar", value);
		node3.set("foo/foo", value);
		node3.set("foo/bar/baz", value);

		System.out.println("Hash: " + hashString(node3));
		System.out.println(mapper.writeValueAsString(node3));

		assertArrayEquals(hash(node1), hash(node2));
		assertNotEquals(hashString(node1), hashString(node3));

		JsonNode storedValue = node3.get("foo/bar");
		assertEquals(value, storedValue);
	}

	private byte[] hash(JsonNode node) throws NoSuchAlgorithmException, JsonProcessingException {
		MessageDigest digest = MessageDigest.getInstance("MD5");
		return digest.digest(mapper.writeValueAsBytes(node));
	}

	private String hashString(JsonNode node) throws NoSuchAlgorithmException, JsonProcessingException {
		return Utils.hexString(hash(node));
	}

}
