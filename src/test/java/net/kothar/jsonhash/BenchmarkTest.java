package net.kothar.jsonhash;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map.Entry;

import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ValueNode;

import net.kothar.jsonhash.ordered.OrderedJsonNodeFactory;
import net.kothar.jsonhash.patricia.PatriciaTrie;
import net.kothar.jsonhash.sorted.SortedHash;

public class BenchmarkTest {

	private static final int	ITERATIONS			= 50000;
	private static final int	WARMUP_ITERATIONS	= 100000;

	private static ObjectMapper	mapper	= new ObjectMapper();
	private static JsonNode		json;

	private static ObjectMapper	orderedMapper	= new ObjectMapper()
		.setNodeFactory(OrderedJsonNodeFactory.instance);
	private static JsonNode		orderedJson;

	@SuppressWarnings("unused")
	private static byte[]		hash;
	private static PatriciaTrie	trie;

	@BeforeClass
	public static void setup() throws IOException, NoSuchAlgorithmException {
		json = mapper.readTree(BenchmarkTest.class.getResource("benchmark.json"));
		orderedJson = orderedMapper.readTree(BenchmarkTest.class.getResource("benchmark.json"));

		System.out.println("Press enter to start > ");
		System.in.read();

		System.out.println("Warming up");
		long start = System.currentTimeMillis();
		for (int i = 0; i < WARMUP_ITERATIONS; i++) {
			hashTrie();
			hashPrebuiltTrie();
			hashSortedJson();
			hashOrderedJson();
		}
		long elapsed = System.currentTimeMillis() - start;

		System.out.println("Warmup: " + WARMUP_ITERATIONS + " iterations in " + elapsed + "ms");
		System.out.println(trie);
	}

	@Test
	public void BenchmarkTrie() throws JsonProcessingException {

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATIONS; i++) {
			hashTrie();
		}
		long elapsed = System.currentTimeMillis() - start;

		System.out.println("Trie: " + ITERATIONS + " iterations in " + elapsed + "ms");
	}

	private static void hashTrie() throws JsonProcessingException {
		trie = new PatriciaTrie();
		visit("", json, trie);
		hash = trie.md5();
	}

	@Test
	public void BenchmarkPrebuiltTrie() throws JsonProcessingException {

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATIONS; i++) {
			hashPrebuiltTrie();
		}
		long elapsed = System.currentTimeMillis() - start;

		System.out.println("Prebuilt Trie: " + ITERATIONS + " iterations in " + elapsed + "ms");
	}

	private static void hashPrebuiltTrie() {
		hash = trie.md5(true);
	}

	private static void visit(String path, JsonNode node, PatriciaTrie trie) throws JsonProcessingException {
		for (Iterator<Entry<String, JsonNode>> i = node.fields(); i.hasNext();) {
			Entry<String, JsonNode> field = i.next();
			String fieldPath = path + field.getKey();

			JsonNode value = field.getValue();
			if (value.isValueNode()) {
				trie.put(fieldPath, (ValueNode) value);
			} else if (value.isArray()) {
				for (int n = 0; n < value.size(); n++) {
					String elementPath = fieldPath + "[" + n + "]/";
					visit(elementPath, value.get(n), trie);
				}
			} else if (value.isObject()) {
				visit(fieldPath + "/", value, trie);
			} else {
				throw new IllegalArgumentException();
			}
		}
	}

	@Test
	public void BenchmarkSortedJson() throws JsonProcessingException, NoSuchAlgorithmException {

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATIONS; i++) {
			hashSortedJson();
		}
		long elapsed = System.currentTimeMillis() - start;

		System.out.println("Sorted Json: " + ITERATIONS + " iterations in " + elapsed + "ms");
	}

	private static void hashSortedJson() throws JsonProcessingException, NoSuchAlgorithmException {
		hash = SortedHash.hash(json);
	}

	@Test
	public void BenchmarkOrderedJson() throws JsonProcessingException, NoSuchAlgorithmException {

		long start = System.currentTimeMillis();
		for (int i = 0; i < ITERATIONS; i++) {
			hashOrderedJson();
		}
		long elapsed = System.currentTimeMillis() - start;

		System.out.println("Ordered Json: " + ITERATIONS + " iterations in " + elapsed + "ms");
	}

	private static void hashOrderedJson() throws JsonProcessingException, NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("MD5");
		hash = digest.digest(orderedMapper.writeValueAsBytes(orderedJson));
	}
}
