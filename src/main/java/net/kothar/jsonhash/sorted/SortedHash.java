package net.kothar.jsonhash.sorted;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.kothar.jsonhash.Utils;

public class SortedHash {

	private MessageDigest	digest;
	private ObjectMapper	mapper;

	SortedHash() throws NoSuchAlgorithmException {
		digest = MessageDigest.getInstance("MD5");
		mapper = new ObjectMapper();
	}

	public static String hashString(ObjectNode node) throws JsonProcessingException {
		return Utils.hexString(hash(node));
	}

	public static byte[] hash(JsonNode node) throws JsonProcessingException {
		try {
			// Collect nodes
			SortedHash hash = new SortedHash();
			hash.visit("/", node);

			return hash.digest();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new UnsupportedOperationException(e);
		}
	}

	private byte[] digest() {
		return digest.digest();
	}

	private void visit(String path, JsonNode node) throws JsonProcessingException, UnsupportedEncodingException {
		if (node.isValueNode()) {
			digest.update(path.getBytes("UTF-8"));
			digest.update(mapper.writeValueAsBytes(node));
			return;
		}

		List<Entry<String, JsonNode>> fields = new ArrayList<>(node.size());
		node.fields().forEachRemaining(fields::add);
		Collections.sort(fields, (a, b) -> a.getKey().compareTo(b.getKey()));

		for (Entry<String, JsonNode> field : fields) {
			String fieldPath = path + field.getKey();
			JsonNode value = field.getValue();

			if (value.isValueNode()) {
				visit(fieldPath, value);
			} else if (value.isArray()) {
				for (int n = 0; n < value.size(); n++) {
					String elementPath = fieldPath + "[" + n + "]/";
					visit(elementPath, value.get(n));
				}
			} else if (value.isObject()) {
				visit(fieldPath + "/", value);
			} else {
				throw new IllegalArgumentException();
			}
		}
	}
}
