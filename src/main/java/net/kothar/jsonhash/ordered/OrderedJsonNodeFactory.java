package net.kothar.jsonhash.ordered;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class OrderedJsonNodeFactory extends JsonNodeFactory {

	private static final long serialVersionUID = 1L;

	public static final OrderedJsonNodeFactory instance = new OrderedJsonNodeFactory();

	@Override
	public ObjectNode objectNode() {
		return new OrderedObjectNode(this);
	}

}
