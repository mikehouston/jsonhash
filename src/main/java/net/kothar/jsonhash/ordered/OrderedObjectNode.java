package net.kothar.jsonhash.ordered;

import java.util.TreeMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class OrderedObjectNode extends ObjectNode {

	public OrderedObjectNode(JsonNodeFactory nc) {
		super(nc, new TreeMap<String, JsonNode>());
	}

}
