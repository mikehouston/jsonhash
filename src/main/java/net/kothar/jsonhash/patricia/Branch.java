package net.kothar.jsonhash.patricia;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

public class Branch implements Node {

	byte			offset		= 0x20;
	Node[]			children	= new Node[0];
	byte[]			value;
	private byte[]	hash;

	Branch() {

	}

	Branch(Node[] children, byte offset, byte[] value) {
		this.children = children;
		this.offset = offset;
		this.value = value;
	}

	Branch(ByteBuffer path, Node child) {
		int index = getIndex(path);
		children[index] = child;
	}

	protected int getIndex(ByteBuffer path) {
		if (path.remaining() == 0) {
			throw new IllegalArgumentException();
		}

		byte branch = path.get();
		int index = branch - offset;
		if (index < 0) {
			throw new IllegalArgumentException();
		}

		if (index >= children.length) {
			Node[] newChildren = new Node[index + 1];
			System.arraycopy(children, 0, newChildren, 0, children.length);
			children = newChildren;
		}
		return index;
	}

	@Override
	public byte[] hash(MessageDigest digest, ByteBuffer buffer, boolean force) {
		if (hash == null || force) {
			for (int i = 0; i < children.length; i++) {
				Node child = children[i];
				if (child != null) {
					buffer.put((byte) (i + offset));
					buffer.put(child.hash(digest, buffer.slice(), force));
				}
			}

			buffer.flip();
			digest.update(buffer);

			if (value != null) {
				digest.update(value);
			} else {
				digest.update(EMPTY);
			}
			hash = digest.digest();
		}
		return hash;
	}

	@Override
	public Node put(ByteBuffer path, byte[] value) {
		hash = null;

		if (path.limit() == 0) {
			this.value = value;
			return this;
		}

		int index = getIndex(path);
		Node child = children[index];
		if (child == null) {
			children[index] = new Leaf(path.slice(), value);
		} else {
			children[index] = child.put(path.slice(), value);
		}
		return this;
	}

	@Override
	public byte[] get(ByteBuffer path) {
		if (path.limit() == 0) {
			return value;
		}

		byte branch = path.get();
		int index = branch - offset;
		if (index < 0 || index >= children.length) {
			return null;
		}

		Node child = children[index];
		if (child == null) {
			return null;
		}
		return child.get(path.slice());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		toString(sb, "", "  ", "");
		return sb.toString();
	}

	@Override
	public void toString(StringBuilder sb, String indent, String increment, String prefix) {
		sb.append(String.format("%s[b %s]: %s\n", indent, prefix, value == null ? null : new String(value)));
		for (int i = 0; i < children.length; i++) {
			Node child = children[i];
			if (child != null) {
				child.toString(sb, indent + increment, increment, new String(new byte[] { (byte) (i + offset) }));
			}
		}
	}

}
