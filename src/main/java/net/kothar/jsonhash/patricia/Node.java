package net.kothar.jsonhash.patricia;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

public interface Node {

	static final byte[] EMPTY = new byte[16];

	public byte[] hash(MessageDigest digest, ByteBuffer buffer, boolean force);

	public Node put(ByteBuffer path, byte[] value);

	public byte[] get(ByteBuffer path);

	public void toString(StringBuilder sb, String indent, String increment, String prefix);

}
