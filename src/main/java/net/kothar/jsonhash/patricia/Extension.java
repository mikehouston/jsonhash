package net.kothar.jsonhash.patricia;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

public class Extension implements Node {

	private byte[]	path;
	private Node	child;
	private byte[]	hash;

	public Extension(ByteBuffer path) {
		this.path = new byte[path.remaining()];
		path.get(this.path);
	}

	public Extension(ByteBuffer path, Node child) {
		this(path);
		this.child = child;
	}

	@Override
	public byte[] hash(MessageDigest digest, ByteBuffer buffer, boolean force) {
		if (hash == null || force) {
			buffer.put(path);
			buffer.put(child.hash(digest, buffer.slice(), force));
			buffer.flip();
			digest.update(buffer);
			hash = digest.digest();
		}
		return hash;
	}

	@Override
	public Node put(ByteBuffer path, byte[] value) {
		hash = null;

		if (path.equals(ByteBuffer.wrap(this.path))) {
			if (child == null) {
				child = new Leaf(ByteBuffer.allocate(0), value);
			} else {
				child = this.child.put(ByteBuffer.allocate(0), value);
			}
			return this;
		}

		int prefixLen = 0;
		while (prefixLen < path.limit() && prefixLen < this.path.length
			&& path.get(prefixLen) == this.path[prefixLen]) {
			prefixLen++;
		}

		Node newNode;
		if (prefixLen == 0) {
			newNode = new Branch(ByteBuffer.wrap(this.path), child);
			newNode.put(path, value);
			return newNode;
		} else if (prefixLen == this.path.length) {
			path.position(prefixLen);
			if (child == null) {
				child = new Leaf(path.slice(), value);
			} else {
				child = child.put(path.slice(), value);
			}
			return this;
		} else {
			ByteBuffer pathPrefix = ByteBuffer.wrap(this.path, 0, prefixLen);
			ByteBuffer pathSuffix = ByteBuffer.wrap(this.path, prefixLen, this.path.length - prefixLen);
			Branch newBranch = new Branch(pathSuffix, child);
			newNode = new Extension(pathPrefix, newBranch);

			path.position(prefixLen);
			newBranch.put(path.slice(), value);
			return newNode;
		}
	}

	@Override
	public byte[] get(ByteBuffer path) {
		if (child == null) {
			return null;
		}

		int prefixLen = 0;
		while (prefixLen < path.limit() && prefixLen < this.path.length
			&& path.get(prefixLen) == this.path[prefixLen]) {
			prefixLen++;
		}
		if (prefixLen < this.path.length) {
			return null;
		}

		path.position(prefixLen);
		return child.get(path.slice());
	}

	@Override
	public void toString(StringBuilder sb, String indent, String increment, String prefix) {
		if (child != null) {
			sb.append(String.format("%s[x %s]\n", indent, prefix));
			child.toString(sb, indent + increment, increment, new String(path));
		}
	}

}
