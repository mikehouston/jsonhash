package net.kothar.jsonhash.patricia;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ValueNode;

import net.kothar.jsonhash.Utils;

public class PatriciaTrie {

	Node			root;
	byte[]			md5;
	ObjectMapper	mapper;

	public PatriciaTrie() {
		mapper = new ObjectMapper();
	}

	public void put(String path, ValueNode value) throws JsonProcessingException {
		byte[] valueBytes = mapper.writeValueAsBytes(value);
		md5 = null;
		if (root == null) {
			root = new Leaf(toBytes(path), valueBytes);
		} else {
			root = root.put(toBytes(path), valueBytes);
		}
	}

	protected ByteBuffer toBytes(String path) {
		try {
			return ByteBuffer.wrap(path.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new UnsupportedOperationException(e);
		}
	}

	public byte[] md5() {
		return md5(false);
	}

	public byte[] md5(boolean force) {
		try {
			if (md5 == null || force) {
				MessageDigest digest = MessageDigest.getInstance("MD5");
				md5 = root.hash(digest, ByteBuffer.allocate(1024), force);
			}
			return md5;
		} catch (NoSuchAlgorithmException e) {
			throw new UnsupportedOperationException(e);
		}
	}

	public ValueNode get(String path) {
		byte[] valueBytes = root.get(toBytes(path));
		if (valueBytes == null) {
			return null;
		}

		try {
			return (ValueNode) mapper.readTree(valueBytes);
		} catch (IOException e) {
			throw new IllegalStateException("Unable to recover value", e);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		root.toString(sb, "", "  ", "/");
		return sb.toString();
	}

	public String md5String() {
		return Utils.hexString(md5());
	}
}
