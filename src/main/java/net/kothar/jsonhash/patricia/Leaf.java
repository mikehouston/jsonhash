package net.kothar.jsonhash.patricia;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

public class Leaf implements Node {

	private byte[]	path;
	private byte[]	value;
	private byte[]	hash;

	public Leaf(ByteBuffer path, byte[] value) {
		this.value = value;
		this.path = new byte[path.remaining()];
		path.get(this.path);
	}

	@Override
	public byte[] hash(MessageDigest digest, ByteBuffer buffer, boolean force) {
		if (hash == null || force) {
			digest.update(path);
			if (value != null) {
				digest.update(value);
			} else {
				digest.update(EMPTY);
			}
			hash = digest.digest();
		}
		return hash;
	}

	@Override
	public Node put(ByteBuffer path, byte[] value) {
		hash = null;

		if (path.equals(ByteBuffer.wrap(this.path))) {
			this.value = value;
			return this;
		}

		int prefixLen = 0;
		while (prefixLen < path.limit() && prefixLen < this.path.length
			&& path.get(prefixLen) == this.path[prefixLen]) {
			prefixLen++;
		}

		Node newNode;
		if (prefixLen == 0) {
			newNode = new Branch();
		} else {
			newNode = new Extension(ByteBuffer.wrap(this.path, 0, prefixLen));
		}
		newNode.put(ByteBuffer.wrap(this.path), this.value);
		newNode.put(path, value);
		return newNode;
	}

	@Override
	public byte[] get(ByteBuffer path) {
		if (path.equals(ByteBuffer.wrap(this.path))) {
			return value;
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		toString(sb, "", "  ", "");
		return sb.toString();
	}

	@Override
	public void toString(StringBuilder sb, String indent, String increment, String prefix) {
		sb.append(
			String.format("%s[l %s%s]: %s\n", indent, prefix, new String(path),
				value == null ? null : new String(value)));
	}

}
