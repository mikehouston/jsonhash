package net.kothar.jsonhash;

public class Utils {

	private static final String hexDigits = "0123456789abcdef";

	public static String hexString(byte[] bytes) {
		char[] chars = new char[bytes.length * 2];
		for (int i = 0; i < bytes.length; i++) {
			byte b = bytes[i];
			chars[i * 2] = hexDigits.charAt((b & 0xF0) >>> 4);
			chars[i * 2 + 1] = hexDigits.charAt(b & 0xF);
		}
		return new String(chars);
	}

}
